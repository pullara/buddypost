const initialState = {
  isLoading: false,
  isLoggedIn: null,
  current: null,
};

export default (state = initialState, action) => {
  switch (action.type) {
    case 'LOADING' :
      return {
        ...state,
        isLoading: true,
      };
    case 'LOGGED_IN':
      return {
        ...state,
        isLoading: false,
        isLoggedIn: true,
        current: action.payload,
      };
    case 'NOT_LOGGED':
      return {
        ...state,
        isLoading: false,
        isLoggedIn: false,
        current: null,
      };
    case 'LOGOUT':
      return {
        ...state,
        ...initialState,
      };
    default:
      return state;
  }
};
