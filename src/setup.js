import React, { Component } from 'react';
import { StatusBar, Platform } from 'react-native';
import Parse from 'parse/react-native';
import { Provider } from 'react-redux';
import { createStore, applyMiddleware } from 'redux';
import ReduxThunk from 'redux-thunk';
import Navigator from './components/Navigator';
import reducers from './reducers';

const store = createStore(reducers, {}, applyMiddleware(ReduxThunk));

export default class Setup extends Component {
  constructor(props) {
    super(props);
    Parse.initialize('buddypost-v4Pru');
    Parse.serverURL = 'https://buddypost.herokuapp.com/parse';
    if (Platform.OS === 'android') {
      StatusBar.setBackgroundColor('#111');
    }
  }

  render() {
    return (
      <Provider store={store}>
        <Navigator />
      </Provider>
    );
  }

}
