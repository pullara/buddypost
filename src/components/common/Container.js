import React, { PropTypes } from 'react';
import {
  View,
  StyleSheet,
} from 'react-native';

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
});

const Container = (props) => {
  return (
    <View style={[styles.container, props.style]}>
      {props.children}
    </View>
  );
};

Container.propTypes = {
  children: PropTypes.any,
  style: PropTypes.oneOfType([
    View.propTypes.style,
    PropTypes.object,
  ]),
};

export default Container;
