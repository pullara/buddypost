import React, { Component, PropTypes } from 'react';
import { StyleSheet, TouchableHighlight, Text, View } from 'react-native';
import Color from 'color';

import Icon from 'react-native-vector-icons/Ionicons';
import config from '../../config';

const styles = StyleSheet.create({
  button: {
    justifyContent: 'center',
    borderRadius: 3,
    marginLeft: 15,
    marginRight: 15,
  },
  buttonText: {
    color: '#fff',
    fontFamily: config.fontFamily.regular,
    fontWeight: '400',
  },
  icon: {
    marginRight: 10,
  },
  iconRight: {
    marginLeft: 10,
  },
});

class Button extends Component {

  getInitialStyle() {
    const getBackgroundColor = () => {
      if (this.props.bgColor) {
        return this.props.bgColor;
      }
      switch (this.props.color) {
        case 'primary':
          return config.colors.primary;
        case 'success':
          return config.colors.success;
        case 'warning':
          return config.colors.warning;
        case 'info':
          return config.colors.info;
        case 'facebook':
          return config.colors.facebook;
        case 'transparent':
          return 'rgba(0,0,0,0)';
        case 'white':
          return '#fff';
        default:
          return config.colors.black;
      }
    };

    const getHeight = () => {
      switch (this.props.size) {
        case 'large':
          return 55;
        case 'small':
          return 35;
        case 'mini':
          return 26;
        default:
          return 44;
      }
    };

    const getPadding = () => {
      switch (this.props.size) {
        case 'large':
          return 20;
        case 'small':
          return 12;
        case 'mini':
          return 10;
        default:
          return 16;
      }
    };

    const getAlign = () => {
      if (this.props.block) {
        return 'stretch';
      } else if (this.props.center) {
        return 'center';
      }
      return 'flex-start';
    };

    const getFontSize = () => {
      switch (this.props.size) {
        case 'large':
          return 20;
        case 'small':
          return 14;
        case 'mini':
          return 12;
        default:
          return 17;
      }
    };

    const styleObj = {
      button: {
        backgroundColor: getBackgroundColor(),
        borderWidth: (this.props.outline) ? 1 : 0,
        borderColor: (this.props.outline && this.props.borderColor) ? this.props.borderColor : '#ccc',
        height: getHeight(),
        paddingHorizontal: getPadding(),
        alignSelf: getAlign(),
        alignItems: this.props.alignText || 'center',
      },
      buttonText: {
        fontSize: getFontSize(),
        color: (this.props.outline || this.props.color === 'transparent' || this.props.color === 'white') ? config.colors.text : '#fff',
        letterSpacing: (this.props.upper) ? 1 : 0,
      },
    };

    return styleObj;
  }

  renderProps() {
    const hoverColor = Color(this.getInitialStyle().button.backgroundColor);
    const underlayColor = this.props.color === 'transparent' ? 'transparent' : hoverColor.mix(Color('white'), 0.9).hexString();
    return {
      ...this.props,
      style: [styles.button, this.getInitialStyle().button, this.props.buttonStyle],
      activeOpacity: this.props.activeOpacity || 0.7,
      underlayColor: this.props.underlayColor ? this.props.underlayColor : underlayColor,
    };
  }

  render() {
    let iconElement;
    const { icon, iconRight, iconColor } = this.props;

    if (icon) {
      let iconStyle = styles.icon;
      if (this.props.iconOnly && !this.props.title) {
        iconStyle = styles.iconOnly;
      } else if (this.props.iconRight) {
        iconStyle = styles.iconRight;
      }

      let iconSize = 18;
      if (this.props.size === 'large') {
        iconSize = 22;
      } else if (this.props.size === 'small') {
        iconSize = 16;
      } else if (this.props.size === 'mini') {
        iconSize = 14;
      }

      iconElement = (
        <Icon
          color={iconColor || this.getInitialStyle().buttonText.color}
          size={iconSize}
          style={iconStyle}
          name={icon}
        />
      );
    }

    return (
      <TouchableHighlight {...this.renderProps()}>
        <View style={{ flexDirection: 'row', justifyContent: 'center', alignItems: 'center' }}>
          { icon && !iconRight && iconElement }
          {
            this.props.title && this.props.title.length > 0 &&
              <Text
                style={
                  [styles.buttonText, this.getInitialStyle().buttonText, this.props.textStyle]
                }
              >
                {(!this.props.upper && this.props.title) || this.props.title.toUpperCase()}
              </Text>
          }
          { icon && iconRight && iconElement }
        </View>
      </TouchableHighlight>
    );
  }

}

Button.defaultProps = {
  block: false,
  iconRight: false,
};

Button.propTypes = {
  title: PropTypes.string,
  color: PropTypes.string,
  bgColor: PropTypes.string,
  borderColor: PropTypes.string,
  outline: PropTypes.bool,
  size: PropTypes.string,
  block: PropTypes.bool,
  center: PropTypes.bool,
  upper: PropTypes.bool,
  activeOpacity: PropTypes.number,
  icon: PropTypes.string,
  iconRight: PropTypes.bool,
  iconColor: PropTypes.string,
  iconOnly: PropTypes.bool,
  underlayColor: PropTypes.string,
  alignText: PropTypes.string,
  buttonStyle: PropTypes.oneOfType([
    View.propTypes.style,
    PropTypes.object,
  ]),
  textStyle: PropTypes.oneOfType([
    Text.propTypes.style,
    PropTypes.object,
  ]),
};

export default Button;
