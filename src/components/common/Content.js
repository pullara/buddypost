import React, { PropTypes } from 'react';
import {
  View,
  ScrollView,
  StyleSheet,
  KeyboardAvoidingView,
} from 'react-native';

const styles = StyleSheet.create({
  content: {
    flex: 1,
    padding: 15,
  },
});

const Content = (props) => {
  const defaultElement = (
    <KeyboardAvoidingView style={[styles.content, props.contentStyle]}>
      {props.children}
    </KeyboardAvoidingView>
  );

  const scrollElement = (
    <ScrollView keyboardShouldPersistTaps>
      {defaultElement}
    </ScrollView>
  );

  const element = props.scroll ? scrollElement : defaultElement;

  return (
    <View style={{ flex: 1, backgroundColor: props.bgColor || '#f8f8f8' }}>
      {element}
    </View>
  );
};

Content.propTypes = {
  contentStyle: View.propTypes.style,
  children: PropTypes.any,
  bgColor: PropTypes.string,
  scroll: PropTypes.bool,
};

export default Content;
