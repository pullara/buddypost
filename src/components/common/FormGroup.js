import React, { PropTypes } from 'react';
import { StyleSheet, View } from 'react-native';

const styles = StyleSheet.create({
  formGroup: {
    marginTop: 15,
  },
});

const FormGroup = (props) => {
  return (
    <View style={[styles.formGroup, props.style]}>
      {props.children}
    </View>
  );
};

FormGroup.propTypes = {
  children: PropTypes.any,
  style: PropTypes.object,
};


export default FormGroup;
