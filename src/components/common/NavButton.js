import React, { PropTypes } from 'react';
import { StyleSheet, TouchableNativeFeedback, TouchableOpacity, View, Platform } from 'react-native';
import Icon from 'react-native-vector-icons/Ionicons';
import config from '../../config';

const styles = StyleSheet.create({
  element: {
    flex: 1,
    justifyContent: 'center',
  },
  button: {
    flex: 1,
    justifyContent: 'center',
    paddingHorizontal: 15,
  },
});

const NavButton = (props) => {
  const { icon, color } = props;
  let iconName = icon;
  if (!props.noPrefix) {
    if (Platform.OS === 'ios') {
      iconName = `ios-${icon}`;
    } else if (Platform.OS === 'android') {
      iconName = `md-${icon}`;
    }
  }

  const Element = Platform.OS === 'android' ? TouchableNativeFeedback : TouchableOpacity;

  return (
    <Element {...props} style={styles.element}>
      <View style={[styles.button, { opacity: props.disabled ? 0.5 : 1 }]}>
        <Icon
          color={color || config.colors.text}
          size={26}
          name={iconName}
        />
      </View>
    </Element>
  );
};

NavButton.propTypes = {
  icon: PropTypes.string.isRequired,
  color: PropTypes.string,
  noPrefix: PropTypes.bool,
  disabled: PropTypes.bool,
};

export default NavButton;
