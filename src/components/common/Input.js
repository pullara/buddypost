import React, { Component, PropTypes } from 'react';
import {
  View,
  TextInput,
  StyleSheet,
  Animated,
} from 'react-native';
import Icon from 'react-native-vector-icons/Ionicons';
import Text from './Text';
import config from '../../config';

const styles = StyleSheet.create({
  container: {
    position: 'relative',
    borderBottomColor: 'rgba(0,0,0,.12)',
    borderBottomWidth: 1,
    marginLeft: 15,
    marginRight: 15,
    marginTop: 5,
  },
  input: {
    flex: 1,
    height: 42,
    fontSize: 17,
    padding: 0,
    fontFamily: config.fontFamily.regular,
    fontWeight: '400',
    color: config.colors.text,
  },
  defaultLabel: {
    backgroundColor: 'transparent',
    color: config.colors.greyText,
    fontSize: 11,
    fontWeight: '400',
    letterSpacing: 0.7,
  },
  floatingLabel: {
    backgroundColor: 'transparent',
    color: config.colors.greyText,
    fontSize: 17,
    fontFamily: config.fontFamily.regular,
    fontWeight: '400',
    position: 'absolute',
    top: 21,
  },
  inputIcon: {
    width: 15,
    marginRight: 10,
  },
  errorLabel: {
    fontSize: 13,
    marginLeft: 15,
    marginRight: 15,
    marginTop: 10,
    color: config.colors.primary,
  },
});

class Input extends Component {
  constructor(props) {
    super(props);

    this.state = {
      isFloated: false,
      isFocused: false,
      animatedValue: new Animated.Value(0),
    };

    this.getStyle = this.getStyle.bind(this);
    this.selectField = this.selectField.bind(this);
    this.deselectField = this.deselectField.bind(this);
  }

  componentWillMount() {
    if (this.props.value && this.props.floatingLabel) {
      this.setState({ isFloated: true });
      Animated.timing(this.state.animatedValue, {
        toValue: 100,
        duration: 0,
      }).start();
    }
  }

  getStyle() {
    const styleObj = {
      container: {
        paddingTop: (this.props.floatingLabel) ? 10 : 0,
        paddingLeft: (this.props.icon) ? 10 : 0,
        paddingRight: (this.props.icon) ? 10 : 0,
        borderBottomColor: (this.state.isFocused) ? 'rgba(0,0,0,.35)' : (this.props.hasError) ? config.colors.primary : 'rgba(0,0,0,.12)',
      },
      label: {
        left: (this.props.icon) ? 35 : 0,
      },
      defaultLabel: {
        marginLeft: (this.props.icon) ? 25 : 0,
      },
      inputContainer: {
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center',
        opacity: (this.props.editable === false) ? 0.3 : 1,
      },
    };
    if (this.state.isFloated) {
      styleObj.label = {
        top: 0,
        fontSize: 11,
        left: (this.props.icon) ? 35 : 0,
        fontWeight: '400',
        color: (this.state.isFocused) ? config.colors.primary : config.colors.greyText,
        letterSpacing: 1,
      };
    }

    return styleObj;
  }

  focus() {
    this.inputRef.focus();
  }

  blur() {
    this.inputRef.blur();
  }

  selectField() {
    if (this.props.floatingLabel) {
      Animated.timing(this.state.animatedValue, {
        toValue: 100,
        duration: 100,
      }).start();
      this.setState({ isFloated: true });
    }

    this.setState({ isFocused: true });
  }

  deselectField(e) {
    this.setState({ isFocused: false });

    if (this.props.floatingLabel) {
      if (e.nativeEvent.text) {
        return;
      }
      Animated.timing(this.state.animatedValue, {
        toValue: 0,
        duration: 100,
      }).start();
      this.setState({ isFloated: false });
    }
  }

  render() {
    const interpolatedLabelPosition = this.state.animatedValue.interpolate({
      inputRange: [0, 100],
      outputRange: [21, 0],
    });

    const { icon } = this.props;
    let iconElement;
    if (icon) {
      iconElement = (
        <Icon
          color={config.colors.greyText}
          size={20}
          style={styles.inputIcon}
          name={icon}
        />
      );
    }

    return (
      <View style={{ flex: 1 }}>
        <View style={[styles.container, this.getStyle().container]}>
          {
            this.props.floatingLabel && this.props.label && this.props.label.length > 0 &&
              <Animated.Text
                style={
                  [styles.floatingLabel, this.getStyle().label, { top: interpolatedLabelPosition }]
                }
              >
                {
                  (!this.state.isFloated && this.props.label) ||
                  (this.state.isFloated && this.props.label.toUpperCase())
                }
              </Animated.Text>
          }
          {
            !this.props.floatingLabel && this.props.label && this.props.label.length > 0 &&
              <Text
                style={[styles.defaultLabel, this.getStyle().defaultLabel]}
              >
                { this.props.label.toUpperCase() }
              </Text>
          }
          <View style={this.getStyle().inputContainer}>
            { icon && iconElement }
            <TextInput
              {...this.props}
              ref={(c) => { this.inputRef = c; }}
              style={[styles.input, this.props.inputStyle]}
              onFocus={this.selectField}
              onEndEditing={this.deselectField}
              autoCapitalize={this.props.autoCapitalize || 'sentences'}
              underlineColorAndroid="transparent"
              placeholderTextColor="#aaa"
            />
            {
              this.props.hasError &&
              <Icon size={18} name="ios-alert-outline" color={config.colors.primary} />
            }
          </View>
        </View>
        {
          this.props.error &&
          <Text accessibilityLiveRegion="polite" style={styles.errorLabel}>{this.props.error}</Text>
        }
      </View>
    );
  }
}

Input.propTypes = {
  floatingLabel: PropTypes.bool,
  label: PropTypes.string,
  icon: PropTypes.string,
  error: PropTypes.string,
  autoCapitalize: PropTypes.string,
  inputStyle: PropTypes.object,
  hasError: PropTypes.bool,
  value: PropTypes.oneOfType([
    PropTypes.string,
    PropTypes.func,
  ]),
};

export default Input;
