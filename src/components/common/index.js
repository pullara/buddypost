import FormGroup from './FormGroup';
import Input from './Input';
import Button from './Button';
import NavButton from './NavButton';
import Container from './Container';
import Content from './Content';
import Text from './Text';
import Navbar from './Navbar';
import LoadingOverlay from './LoadingOverlay';

const Common = {
  FormGroup,
  Input,
  Button,
  NavButton,
  Container,
  Content,
  Text,
  Navbar,
  LoadingOverlay,
};

module.exports = Common;
