import React, { PropTypes } from 'react';
import {
  Text as TextView,
  StyleSheet,
} from 'react-native';
import config from '../../config';

const styles = StyleSheet.create({
  text: {
    fontFamily: config.fontFamily.regular,
  },
});

const Text = (props) => {
  return (
    <TextView {...props} style={[styles.text, props.style]}>
      {props.children}
    </TextView>
  );
};

Text.propTypes = {
  children: PropTypes.any,
  style: PropTypes.oneOfType([
    TextView.propTypes.style,
    PropTypes.object,
  ]),
};


export default Text;
