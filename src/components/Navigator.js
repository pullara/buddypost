import React from 'react';
import { StyleSheet, Platform } from 'react-native';
import { Scene, Router, Actions } from 'react-native-router-flux';
import { NavButton, Navbar } from './common';
import config from '../config';

import Launch from './main/Launch';
import Authenticate from './auth/Authenticate';
import Signin from './auth/Signin';
import Signup from './auth/Signup';
import Home from './main/Home';
import Profile from './account/Profile';
import EditProfile from './account/Edit';
import Addresses from './account/Addresses';
import MyOrders from './account/Orders';

const styles = StyleSheet.create({
  text: {
    fontFamily: config.fontFamily.regular,
  },
  title: {
    fontFamily: config.fontFamily.medium,
    fontSize: 18,
    textAlign: 'center',
  },
  defaultScene: {
    paddingTop: config.navbarHeight,
  },
  defaultNav: {
    backgroundColor: '#fff',
    borderBottomWidth: 0,
    shadowColor: '#000000',
    shadowOpacity: 1,
    elevation: 2,
  },
  buttonText: {
    padding: 10,
    color: config.colors.text,
    fontSize: 14,
    letterSpacing: 0.7,
  },
  transparentNav: {

  },
});

const renderButton = {
  back: (nav, icon = 'arrow-back') => {
    if (nav.navigationState.index === 0) {
      return null;
    }
    return (
      <NavButton icon={icon} onPress={() => Actions.pop()} />
    );
  },
  menu: () => {
    return (
      // To fix: add toggle
      <NavButton
        icon="md-menu"
        noPrefix
        onPress={() => {
          Actions.refresh({ key: 'home', open: true });
        }}
      />
    );
  },
};

const defaultConfig = {
  navBar: Navbar,
  renderBackButton: nav => renderButton.back(nav),
  rightButtonTextStyle: [styles.text, styles.buttonText],
  leftButtonTextStyle: [styles.text, styles.buttonText],
  titleStyle: styles.title,
  sceneStyle: styles.defaultScene,
  navigationBarStyle: styles.defaultNav,
  ...Platform.select({
    android: {
      panHandlers: null,
    },
  }),
};

const Navigator = () => {
  return (
    <Router {...defaultConfig}>
      <Scene key="launch" component={Launch} hideNavBar />

      <Scene key="auth">
        <Scene
          key="landing"
          component={Authenticate}
          hideNavBar
          sceneStyle={{ paddingTop: 0 }}
        />
        <Scene key="signup" component={Signup} hideNavBar={false} />
        <Scene key="login" component={Signin} hideNavBar={false} />
      </Scene>

      <Scene key="main">
        <Scene
          key="home"
          component={Home}
          renderBackButton={false}
          renderLeftButton={nav => renderButton.menu(nav)}
        />

        <Scene key="profile" component={Profile} />
        <Scene key="editProfile" component={EditProfile} renderBackButton={false} />
        <Scene key="addresses" component={Addresses} />
        <Scene key="myOrders" component={MyOrders} />

      </Scene>
    </Router>
  );
};

export default Navigator;
