import React, { Component, PropTypes } from 'react';
import {
  View,
  StyleSheet,
} from 'react-native';
import { connect } from 'react-redux';
import SideMenu from './SideMenu';

import { logOutUser } from '../../actions';
import { Button, Text, NavButton } from '../common';

const styles = StyleSheet.create({
  container: {
    justifyContent: 'center',
    alignItems: 'center',
    flex: 1,
  },
});

class Home extends Component {
  static renderRightButton() {
    return <NavButton icon="search" />;
  }

  static title() {
    return 'Buddypost';
  }

  constructor(props) {
    super(props);
    this.state = {};
  }

  render() {
    return (
      <SideMenu navigationState={this.props.navigationState}>
        <View style={styles.container}>
          <Text>{this.props.currentUser.get('username')}</Text>
          <Button onPress={() => this.props.logOutUser()} center title="Logout" />
        </View>
      </SideMenu>
    );
  }
}

Home.propTypes = {
  logOutUser: PropTypes.func,
  currentUser: PropTypes.object,
  navigationState: PropTypes.object,
};

const mapStateToProps = store => ({
  currentUser: store.user.current,
});

export default connect(mapStateToProps, { logOutUser })(Home);
