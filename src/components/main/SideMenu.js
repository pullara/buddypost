import React from 'react';
import Drawer from 'react-native-drawer';
import { Actions } from 'react-native-router-flux';
import Menu from './Menu';

const SideMenu = (props) => {
  const state = props.navigationState;
  return (
    <Drawer
      type="overlay"
      open={state.open}
      onOpen={() => Actions.refresh({ key: state.key, open: true })}
      onClose={() => Actions.refresh({ key: state.key, open: false })}
      ref={(ref) => { this.drawer = ref; }}
      content={<Menu />}
      tweenDuration={150}
      openDrawerOffset={0.25}
      tapToClose
      panOpenMask={0.2}
      tweenHandler={ratio => ({
        main: {
          opacity: 1,
        },
        drawer: {
          elevation: ratio * 4,
          shadowColor: '#000000',
          shadowOpacity: ratio * 0.1,
          shadowOffset: {
            width: 5,
          },
        },
        mainOverlay: {
          opacity: ratio / 2,
          backgroundColor: 'black',
        },
      })}
    >
      {props.children}
    </Drawer>
  );
};

SideMenu.propTypes = {
  children: React.PropTypes.any,
  navigationState: React.PropTypes.object,
};

export default SideMenu;
