import React, { Component, PropTypes } from 'react';
import {
  View,
  ActivityIndicator,
  StyleSheet,
} from 'react-native';
import { connect } from 'react-redux';
import { checkCurrentUser } from '../../actions';

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    justifyContent: 'center',
    alignItems: 'center',
  },
});

class Launch extends Component {
  componentWillMount() {
    this.props.checkCurrentUser();
  }

  componentWillUpdate() {
    this.props.checkCurrentUser();
  }

  render() {
    return (
      <View style={styles.container}>
        <ActivityIndicator size="large" />
      </View>
    );
  }
}

Launch.propTypes = {
  checkCurrentUser: PropTypes.func,
};

export default connect(null, { checkCurrentUser })(Launch);
