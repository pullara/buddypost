import React from 'react';
import {
  View,
  StyleSheet,
  Image,
} from 'react-native';
import { List, ListItem } from 'react-native-elements';
import { connect } from 'react-redux';
import { Actions } from 'react-native-router-flux';
import { Text, Content } from '../common';
import { logOutUser } from '../../actions';
import config from '../../config';
import i18n from '../../i18n';
import bgImage from '../../assets/img/splash.png';

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: 'white',
  },
  listContainer: {
    marginTop: 0,
    borderTopWidth: 0,
    borderBottomWidth: 0,
  },
  listItem: {
    marginLeft: 0,
    paddingHorizontal: 15,
    paddingTop: 12,
    paddingBottom: 12,
  },
  listTitle: {
    fontFamily: config.fontFamily.regular,
    color: config.colors.text,
    fontSize: 16,
  },
  imageBg: {
    backgroundColor: config.colors.primary,
    position: 'relative',
    height: 175,
    justifyContent: 'flex-end',
    padding: 20,
  },
  image: {
    position: 'absolute',
    top: 0,
    right: 0,
    bottom: 0,
    left: 0,
    width: null,
    height: 175,
    opacity: 0.7,
  },
  fullName: {
    fontFamily: config.fontFamily.medium,
    fontSize: 18,
    backgroundColor: 'transparent',
    color: '#fff',
  },
  email: { backgroundColor: 'transparent', color: '#eee' },
});

const Menu = (props) => {
  const list = [
    {
      title: i18n('orders'),
      icon: 'ios-list-box',
      onPress: () => Actions.myOrders(),
    },
    {
      title: i18n('profile'),
      icon: 'ios-person',
      onPress: () => Actions.profile(),
    },
    {
      title: i18n('myAddresses'),
      icon: 'ios-pin',
      onPress: () => Actions.addresses(),
    },
    {
      title: i18n('paymentMethods'),
      icon: 'ios-cash',
      onPress: () => Actions.paymentMethods(),
    },
    {
      title: i18n('settings'),
      icon: 'ios-settings',
      hideChevron: true,
    },
    {
      title: i18n('logOut'),
      icon: 'ios-power',
      hideChevron: true,
      onPress: () => props.logOutUser(),
    },
  ];
  return (
    <Content scroll contentStyle={{ padding: 0 }} bgColor="#fff">
      <View style={styles.container}>
        <View style={styles.imageBg}>
          <Image
            style={styles.image}
            source={bgImage}
            resizeMode="cover"
          />
          <Text style={styles.fullName}>{props.currentUser.get('firstName') + ' ' + props.currentUser.get('lastName')}</Text>
          <Text style={styles.email}>{props.currentUser.get('email')}</Text>
        </View>
        <List containerStyle={styles.listContainer}>
          {
            list.map((item, i) => (
              <ListItem
                {...item}
                containerStyle={styles.listItem}
                titleStyle={styles.listTitle}
                key={i}
                title={item.title}
                leftIcon={{
                  name: item.icon,
                  type: 'ionicon',
                  color: config.colors.greyText,
                  style: {
                    width: 30,
                  },
                }}
                rightIcon={{
                  name: 'ios-arrow-forward',
                  type: 'ionicon',
                  color: config.colors.greyText,
                }}
                underlayColor="rgba(47, 50, 53, .1)"
              />
            ))
          }
        </List>
      </View>
    </Content>
  );
};

Menu.propTypes = {
  currentUser: React.PropTypes.object,
};

const mapStateToProps = store => ({
  currentUser: store.user.current,
});

export default connect(mapStateToProps, { logOutUser })(Menu);
