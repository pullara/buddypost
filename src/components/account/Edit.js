import React, { Component, PropTypes } from 'react';
import {
  View,
  StyleSheet,
} from 'react-native';
import { connect } from 'react-redux';
import { Actions } from 'react-native-router-flux';
import isEqual from 'lodash.isequal';
import dismissKeyboard from 'react-native/Libraries/Utilities/dismissKeyboard';
import { Container, Content, Text, NavButton, Button, LoadingOverlay } from '../common';
import t from '../form';
import i18n from '../../i18n';
import config from '../../config';
import { updateProfile } from '../../actions';

const Form = t.form.Form;
const ProfileForm = t.struct({
  name: t.struct({
    firstName: t.String,
    lastName: t.String,
  }),
  email: t.Email,
  phone: t.maybe(t.refinement(t.Number, (p) => {
    return p.toString().length >= 8;
  })),
});

const styles = StyleSheet.create({
  container: {
    padding: 0,
  },
  box: {
    backgroundColor: '#fff',
    paddingHorizontal: 15,
    paddingTop: 20,
    paddingBottom: 20,
    borderColor: '#eee',
    borderBottomWidth: 1,
    borderTopWidth: 1,
    marginBottom: 10,
  },
  miniBox: {
    backgroundColor: '#fff',
    borderColor: '#eee',
    borderBottomWidth: 1,
    borderTopWidth: 1,
    marginBottom: 10,
  },
  heading: {
    paddingHorizontal: 30,
    paddingTop: 15,
    paddingBottom: 15,
  },
  headingText: {
    fontSize: 18,
    color: config.colors.greyText,
  },
  fullButton: {
    marginLeft: 0,
    marginRight: 0,
    paddingHorizontal: 30,
  },
});

class Profile extends Component {
  static title() {
    return i18n('editProfile');
  }

  static renderBackButton() {
    return <NavButton icon="md-close" noPrefix onPress={() => Actions.pop()} />;
  }

  constructor(props) {
    super(props);
    const currentUser = this.props.currentUser;
    this.state = {
      value: {
        name: {
          firstName: currentUser.get('firstName'),
          lastName: currentUser.get('lastName'),
        },
        email: currentUser.get('email'),
        phone: currentUser.get('phoneNumber'),
      },
    };
  }

  componentDidMount() {
    Actions.refresh({ renderRightButton: () => this.renderRightButton() });
  }

  onChange(value) {
    this.setState({ value });
  }

  onPress() {
    const value = this.form.getValue();
    const currentUser = this.props.currentUser;

    if (value) {
      const oldValue = {
        name: {
          firstName: currentUser.get('firstName'),
          lastName: currentUser.get('lastName'),
        },
        email: currentUser.get('email'),
        phone: currentUser.get('phoneNumber'),
      };

      const newValue = {
        name: {
          firstName: this.state.value.name.firstName,
          lastName: this.state.value.name.lastName,
        },
        email: this.state.value.email,
        phone: this.state.value.phone ? value.phone.toString() : undefined,
      };

      if (!isEqual(oldValue, newValue)) {
        dismissKeyboard();
        this.props.updateProfile(currentUser, this.state.value);
      }
    }
  }

  getFormOptions() {
    return {
      auto: 'none',
      fields: {
        name: {
          stylesheet: t.doubleForm,
          fields: {
            firstName: {
              label: i18n('firstName'),
              autoCorrect: false,
              returnKeyType: 'next',
              blurOnSubmit: false,
              onSubmitEditing: () => this.form.getComponent('name').refs.lastName.refs.input.focus(),
            },
            lastName: {
              label: i18n('lastName'),
              autoCorrect: false,
              returnKeyType: 'next',
              blurOnSubmit: false,
              onSubmitEditing: () => this.form.getComponent('phone').refs.input.focus(),
            },
          },
        },
        email: {
          label: 'Email',
          autoCorrect: false,
          keyboardType: 'email-address',
          autoCapitalize: 'none',
          returnKeyType: 'next',
          editable: false,
        },
        phone: {
          label: i18n('phoneNumber'),
          keyboardType: 'numeric',
          returnKeyType: 'done',
        },
      },
    };
  }

  renderRightButton() {
    return <NavButton icon="md-checkmark" noPrefix onPress={() => this.onPress()} />;
  }

  render() {
    const formOptions = () => this.getFormOptions();
    return (
      <Container>
        <Content contentStyle={styles.container} scroll>

          <View>
            <View style={styles.heading}>
              <Text style={styles.headingText}>{i18n('contactInfo')}</Text>
            </View>
            <View style={styles.box}>
              <Form
                ref={(c) => { this.form = c; }}
                type={ProfileForm}
                options={formOptions}
                value={this.state.value}
                onChange={e => this.onChange(e)}
              />
            </View>
          </View>

          <View hidden>
            <View style={styles.heading}>
              <Text style={styles.headingText}>Password</Text>
            </View>
            <View style={styles.miniBox}>
              <Button
                title={i18n('editPassword')}
                buttonStyle={styles.fullButton}
                iconColor={config.colors.greyText}
                icon="ios-lock"
                color="white"
                alignText="flex-start"
                underlayColor="rgba(47, 50, 53, .05)"
                textStyle={{ fontSize: 16 }}
                onPress={() => console.log('ciao')}
                block
              />
            </View>
          </View>
        </Content>
        { this.props.isLoading && <LoadingOverlay /> }
      </Container>
    );
  }
}

Profile.propTypes = {
  isLoading: PropTypes.bool,
  updateProfile: PropTypes.func,
  currentUser: PropTypes.object,
};

const mapStateToProps = store => ({
  currentUser: store.user.current,
  isLoading: store.user.isLoading,
});

export default connect(mapStateToProps, { updateProfile })(Profile);
