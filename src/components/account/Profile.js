import React, { Component } from 'react';
import {
  View,
  StyleSheet,
} from 'react-native';
import { connect } from 'react-redux';
import { Actions } from 'react-native-router-flux';
import Icon from 'react-native-vector-icons/Ionicons';
import { Container, Content, Text, NavButton } from '../common';
import i18n from '../../i18n';
import config from '../../config';

const styles = StyleSheet.create({
  container: {
    padding: 0,
  },
  heading: {
    paddingHorizontal: 30,
    paddingTop: 15,
    paddingBottom: 15,
  },
  headingText: {
    fontSize: 18,
    color: config.colors.greyText,
  },
  listItem: {
    flex: 1,
    flexDirection: 'row',
    paddingHorizontal: 30,
    padding: 10,
  },
  listIcon: {
    marginRight: 15,
  },
  listDetails: {
  },
  listTitle: {
    color: config.colors.greyText,
    fontSize: 14,
  },
  listValue: {
    color: config.colors.text,
    fontSize: 16,
  },
});

class Profile extends Component {
  static renderRightButton() {
    return <NavButton icon="md-create" noPrefix onPress={() => Actions.editProfile()} />;
  }

  static title() {
    return i18n('myProfile');
  }

  render() {
    const currentUser = this.props.currentUser;
    const list = [
      {
        title: i18n('fullName'),
        value: currentUser.get('firstName') + ' ' + currentUser.get('lastName'),
        icon: 'ios-contact',
      },
      {
        title: i18n('emailAddress'),
        value: currentUser.get('email'),
        icon: 'ios-mail',
      },
      {
        title: i18n('phoneNumber'),
        value: currentUser.get('phoneNumber') || i18n('notSet'),
        icon: 'ios-call',
      },
    ];
    return (
      <Container>
        <Content contentStyle={styles.container} scroll>

          <View style={styles.heading}>
            <Text style={styles.headingText}>{i18n('contactInfo')}</Text>
          </View>

          <View>
            {
              list.map((item, i) => (
                <View style={styles.listItem} key={i}>
                  {
                    item.icon &&
                    <View style={styles.listIcon}>
                      <Icon name={item.icon} size={26} color={config.colors.greyText} />
                    </View>
                  }
                  <View style={styles.listDetails}>
                    <Text style={styles.listTitle}>{item.title}</Text>
                    <Text style={styles.listValue}>{item.value}</Text>
                  </View>
                </View>
              ))
            }
          </View>

        </Content>
      </Container>
    );
  }
}

Profile.propTypes = {
  currentUser: React.PropTypes.object,
};

const mapStateToProps = store => ({
  currentUser: store.user.current,
});

export default connect(mapStateToProps)(Profile);
