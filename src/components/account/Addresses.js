/* @flow */

import React, { Component, PropTypes } from 'react';
import {
  View,
  Text,
  StyleSheet,
} from 'react-native';
import ActionButton from 'react-native-action-button';
import i18n from '../../i18n';

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
  },
});

class Addresses extends Component {
  static title() {
    return i18n('myAddresses');
  }

  constructor(props) {
    super(props);
    this.state = {};
  }

  render() {
    return (
      <View style={styles.container}>
        <Text>Im the MyComponent component</Text>
        <ActionButton
          buttonColor="rgba(231,76,60,1)"
          onPress={() => {}}
        />
      </View>
    );
  }
}

Addresses.propTypes = {

};

export default Addresses;
