import React from 'react';
import { View } from 'react-native';
import { connect } from 'react-redux';
import Navigator from './Navigator';
import { LoadingOverlay } from './common';

const App = props => (
  <View style={{ flex: 1 }}>
    <Navigator />
    { props.isLoading && <LoadingOverlay /> }
  </View>
);

App.propTypes = {
  isLoading: React.PropTypes.bool,
};

const mapStateToProps = store => ({
  isLoading: store.user.isLoading,
});

export default connect(mapStateToProps, null)(App);
