import t from 'tcomb-form-native/lib';
import i18n from 'tcomb-form-native/lib/i18n/en';
import { checkbox, select, datepicker, struct, list } from 'tcomb-form-native/lib/templates/bootstrap';
import stylesheet from 'tcomb-form-native/lib/stylesheets/bootstrap';
import textbox from './textbox';

const templates = {
  textbox,
  checkbox,
  select,
  datepicker,
  struct,
  list,
};

t.form.Form.templates = templates;
t.form.Form.stylesheet = stylesheet;
t.form.Form.i18n = i18n;

t.Email = t.refinement(t.String, (e) => {
  const re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
  return re.test(e);
});

t.doubleForm = Object.assign({}, t.form.Form.stylesheet);
t.doubleForm.fieldset = {
  flexDirection: 'row',
};
t.doubleForm.formGroup.normal.flex = 1;
t.doubleForm.formGroup.error.flex = 1;

export default t;
