import React, { Component, PropTypes } from 'react';
import { connect } from 'react-redux';
import { Button, FormGroup, Container, Content, LoadingOverlay } from '../common';
import t from '../form';
import i18n from '../../i18n';
import { registerUser } from '../../actions';

const Form = t.form.Form;

const SignupForm = t.struct({
  name: t.struct({
    firstName: t.String,
    lastName: t.String,
  }),
  email: t.Email,
  password: t.refinement(t.String, (p) => {
    return p.length >= 6;
  }),
});

class Signup extends Component {
  static title() {
    return i18n('createAccount');
  }

  constructor(props) {
    super(props);
    this.state = {
      value: {},
      hasBeenSubmitted: false,
    };
  }

  onChange(value) {
    this.setState({ value });
    if (this.state.hasBeenSubmitted) {
      this.form.validate();
    }
  }

  onPress() {
    this.setState({ hasBeenSubmitted: true });
    const value = this.form.getValue();
    if (value) {
      this.props.registerUser(value);
    }
  }

  getFormOptions() {
    return {
      auto: 'none',
      fields: {
        name: {
          stylesheet: t.doubleForm,
          fields: {
            firstName: {
              label: i18n('firstName'),
              autoCorrect: false,
              autoFocus: true,
              returnKeyType: 'next',
              blurOnSubmit: false,
              onSubmitEditing: () => this.form.getComponent('name').refs.lastName.refs.input.focus(),
            },
            lastName: {
              label: i18n('lastName'),
              autoCorrect: false,
              returnKeyType: 'next',
              blurOnSubmit: false,
              onSubmitEditing: () => this.form.getComponent('email').refs.input.focus(),
            },
          },
        },
        email: {
          label: 'Email',
          autoCorrect: false,
          keyboardType: 'email-address',
          autoCapitalize: 'none',
          returnKeyType: 'next',
          blurOnSubmit: false,
          onSubmitEditing: () => this.form.getComponent('password').refs.input.focus(),
        },
        password: {
          label: 'Password',
          password: true,
          secureTextEntry: true,
          error: i18n('passwordTooShort'),
          returnKeyType: 'done',
        },
      },
    };
  }

  render() {
    const formOptions = () => this.getFormOptions();
    return (
      <Container>

        <Content scroll>

          <Form
            ref={(c) => { this.form = c; }}
            type={SignupForm}
            options={formOptions}
            value={this.state.value}
            onChange={e => this.onChange(e)}
          />

          <FormGroup style={{ marginTop: 30 }}>
            <Button
              block
              onPress={() => this.onPress()}
              title={i18n('signup')}
              color="primary"
            />
          </FormGroup>

        </Content>
        { this.props.isLoading && <LoadingOverlay /> }
      </Container>
    );
  }
}

Signup.propTypes = {
  isLoading: PropTypes.bool,
  registerUser: PropTypes.func,
};

const mapStateToProps = store => ({
  isLoading: store.user.isLoading,
});

export default connect(mapStateToProps, { registerUser })(Signup);
