import React from 'react';
import { Actions } from 'react-native-router-flux';
import { Image, StyleSheet, View, Dimensions, Platform } from 'react-native';
import { connect } from 'react-redux';
import { Row, Grid } from 'react-native-easy-grid';
import { Text, Button, FormGroup, Container, Content, LoadingOverlay } from '../common';
import { fbLogin } from '../../actions';
import i18n from '../../i18n';
import bgImage from '../../assets/img/splash.png';
import config from '../../config';

const styles = StyleSheet.create({
  container: {
    justifyContent: 'center',
  },
  rowBg: {
    backgroundColor: config.colors.primary,
    paddingTop: 20,
    ...Platform.select({
      ios: {
        paddingTop: 20,
      },
      android: {
        paddingTop: 10,
      },
    }),
    overflow: 'hidden',
  },
  image: {
    flex: 1,
    width: Dimensions.get('window').width,
    ...Platform.select({
      ios: {
        height: null,
      },
      android: {
        height: Dimensions.get('window').height * 0.65,
      },
    }),
  },
});

const Authenticate = (props) => {
  return (
    <Container>
      <Grid>
        <Row size={60} style={styles.rowBg}>
          <Image
            style={styles.image}
            source={bgImage}
            resizeMode="cover"
          />
        </Row>

        <Row size={40}>
          <Content bgColor="#fff">
            <View style={{ flex: 1, justifyContent: 'center' }}>
              <FormGroup>
                <Button
                  title={i18n('fbLogin')}
                  icon="logo-facebook"
                  color="facebook"
                  onPress={() => props.fbLogin()}
                  block
                />
              </FormGroup>

              <FormGroup>
                <Button
                  onPress={() => Actions.signup()}
                  title={i18n('createAccount')}
                  block
                  color="primary"
                />
              </FormGroup>

              <FormGroup style={{ flexDirection: 'row', alignItems: 'center', justifyContent: 'center', marginTop: 20 }}>
                <Text style={{ textAlign: 'center' }}>{i18n('alreadyRegistered')}</Text>
                <Button
                  onPress={() => Actions.login()}
                  title={i18n('login')}
                  size="small"
                  color="transparent"
                  outline
                />
              </FormGroup>
            </View>
          </Content>
        </Row>
      </Grid>
      { props.isLoading && <LoadingOverlay /> }
    </Container>
  );
};

Authenticate.propTypes = {
  isLoading: React.PropTypes.bool,
};

const mapStateToProps = store => ({
  isLoading: store.user.isLoading,
});

export default connect(mapStateToProps, { fbLogin })(Authenticate);
