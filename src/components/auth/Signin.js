import React, { Component, PropTypes } from 'react';
import { connect } from 'react-redux';
import { Button, FormGroup, Container, Content, LoadingOverlay } from '../common';
import { logInUser } from '../../actions';
import t from '../form';
import i18n from '../../i18n';

const Form = t.form.Form;

const SigninForm = t.struct({
  email: t.Email,
  password: t.refinement(t.String, (p) => {
    return p.length >= 6;
  }),
});

class Signin extends Component {
  static title() {
    return i18n('login');
  }

  constructor(props) {
    super(props);

    this.state = {
      value: {},
      hasBeenSubmitted: false,
    };
  }

  onChange(value) {
    this.setState({ value });
    if (this.state.hasBeenSubmitted) {
      this.form.validate();
    }
  }

  onPress() {
    this.setState({ hasBeenSubmitted: true });
    const value = this.form.getValue();
    if (value) {
      this.props.logInUser(value);
    }
  }

  getFormOptions() {
    return {
      auto: 'none',
      fields: {
        email: {
          label: 'Email',
          autoCorrect: false,
          keyboardType: 'email-address',
          autoCapitalize: 'none',
          returnKeyType: 'next',
          blurOnSubmit: false,
          onSubmitEditing: () => this.form.getComponent('password').refs.input.focus(),
          config: {
            icon: 'ios-mail',
          },
        },
        password: {
          label: 'Password',
          password: true,
          secureTextEntry: true,
          error: i18n('passwordTooShort'),
          returnKeyType: 'done',
          config: {
            icon: 'ios-lock',
          },
        },
      },
    };
  }

  render() {
    const formOptions = () => this.getFormOptions();
    return (
      <Container>

        <Content scroll>

          <Form
            ref={(c) => { this.form = c; }}
            type={SigninForm}
            options={formOptions}
            value={this.state.value}
            onChange={e => this.onChange(e)}
          />

          <FormGroup style={{ marginTop: 30 }}>
            <Button block color="primary" onPress={() => this.onPress()} title={i18n('login')} />
          </FormGroup>

        </Content>
        { this.props.isLoading && <LoadingOverlay /> }
      </Container>
    );
  }
}

Signin.propTypes = {
  logInUser: PropTypes.func,
  isLoading: PropTypes.bool,
};

const mapStateToProps = store => ({
  isLoading: store.user.isLoading,
});

export default connect(mapStateToProps, { logInUser })(Signin);
