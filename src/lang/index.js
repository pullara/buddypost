import en from './en';
import it from './it';

const translations = {
  en,
  it,
};

export default translations;
