import { Platform } from 'react-native';
import Color from 'color';

const config = {
  fontFamily: {
    ...Platform.select({
      ios: {
        regular: 'Catamaran',
        medium: 'Catamaran Medium',
      },
      android: {
        regular: 'catamaran',
        medium: 'catamaran_medium',
      },
    }),
  },
  colors: {
    text: '#2f3235',
    greyText: Color('#2f3235').lighten(1.8).hexString(),
    primary: '#FF3A51',
    success: '#07b57a',
    info: '#3AAED8',
    warning: '#FFD311',
    black: '#262626',
    facebook: '#3b5998',
  },
  ...Platform.select({
    ios: {
      navbarHeight: 64,
    },
    android: {
      navbarHeight: 54,
    },
  }),
};

module.exports = config;
