import Parse from 'parse/react-native';
import { Alert } from 'react-native';
import { Actions, ActionConst } from 'react-native-router-flux';
import { LoginManager, AccessToken, GraphRequest, GraphRequestManager } from 'react-native-fbsdk';
import i18n from '../i18n';

const loginUserSuccess = (dispatch, user) => {
  dispatch({
    type: 'LOGGED_IN',
    payload: user,
  });

  Actions.main({ type: ActionConst.REPLACE });
};

const checkCurrentUser = () => (dispatch) => {
  Parse.User.currentAsync().then((user) => {
    if (user) {
      loginUserSuccess(dispatch, user);
    } else {
      dispatch({ type: 'NOT_LOGGED' });
      Actions.auth({ type: ActionConst.REPLACE });
    }
  });
};

const logInUser = ({ email, password }) => (dispatch) => {
  dispatch({ type: 'LOADING' });
  Parse.User.logIn(email, password, {
    success: (user) => {
      loginUserSuccess(dispatch, user);
    },
    error: (user, error) => {
      Alert.alert(i18n('error'), error.message);
      dispatch({ type: 'NOT_LOGGED' });
    },
  });
};

const registerUser = data => (dispatch) => {
  dispatch({ type: 'LOADING' });
  const user = new Parse.User();
  const { email, password } = data;
  const { firstName, lastName } = data.name;
  user.set('username', email);
  user.set('password', password);
  user.set('email', email);
  user.set('firstName', firstName);
  user.set('lastName', lastName);

  user.signUp(null, {
    success: () => dispatch(logInUser(email, password)),
    error: (usr, error) => {
      Alert.alert(i18n('error'), error.message);
      dispatch({ type: 'NOT_LOGGED' });
    },
  });
};

const updateProfile = (user, data) => (dispatch) => {
  dispatch({ type: 'LOADING' });
  const { phone } = data;
  const { firstName, lastName } = data.name;
  user.set('firstName', firstName);
  user.set('lastName', lastName);
  user.set('phoneNumber', phone);

  user.save().then((usr) => {
    usr.fetch({
      success: (u) => {
        dispatch({ type: 'LOGGED_IN', payload: u });
        Actions.pop({ refresh: {} });
      },
    });
  },
  ).then(
    () => {
      // alert('Profile Updated');
    },
    () => {
      // alert('Something went wrong');
    },
  );
};

async function getProfile(accessToken) {
  return new Promise((resolve, reject) => {
    const profileRequest = new GraphRequest(
      '/me',
      {
        httpMethod: 'GET',
        version: 'v2.5',
        parameters: {
          fields: {
            string: 'id, email, first_name, last_name',
          },
        },
        accessToken,
      },
      (err, result) => {
        if (err) {
          reject(err);
          return;
        }
        resolve(result);
      },
    );
    new GraphRequestManager()
      .addRequest(profileRequest)
      .start();
  });
}

async function ParseFacebookLogin(data) {
  return new Promise((resolve, reject) => {
    let expdate = new Date(data.expirationTime);
    expdate = expdate.toISOString();

    const authData = {
      id: data.userID,
      access_token: data.accessToken,
      expiration_date: expdate,
    };

    Parse.FacebookUtils.logIn(authData, {
      success: user => resolve(user),
      error: (user, error) => reject((error && error.error) || error),
    });
  });
}

async function logInWithFacebook(data, dispatch) {
  await ParseFacebookLogin(data).catch(err => console.error(err));

  const user = await Parse.User.currentAsync();
  if (user) {
    if (!user.existed()) {
      const profile = await getProfile(data.accessToken);
      user.set('facebook_id', profile.id);
      user.set('firstName', profile.first_name);
      user.set('lastName', profile.last_name);
      user.set('email', profile.email);
      await user.save();
      console.log('User signed up and logged in through Facebook!');
    } else {
      console.log('User logged in through Facebook!');
    }

    loginUserSuccess(dispatch, user);
  }
}

const fbLogin = () => (dispatch) => {
  dispatch({ type: 'LOADING' });

  LoginManager.logInWithReadPermissions(['public_profile', 'email'])
  .then((result) => {
    if (result.isCancelled) {
      dispatch({ type: 'NOT_LOGGED' });
    } else {
      AccessToken.getCurrentAccessToken().then(
        data => logInWithFacebook(data, dispatch),
      );
    }
  }).catch((err) => {
    Alert.alert(i18n('error'), err.message);
    dispatch({ type: 'NOT_LOGGED' });
  });
};

const logOutUser = () => (dispatch) => {
  Alert.alert(
    i18n('confirmation'),
    i18n('confirmLogout'),
    [
      { text: 'No' },
      {
        text: i18n('yes'),
        onPress: () => {
          Parse.User.logOut().then(() => {
            Actions.auth({ type: ActionConst.REPLACE });
            dispatch({ type: 'LOGOUT' });
          });
        },
      },
    ],
  );
};

const auth = {
  checkCurrentUser,
  registerUser,
  logInUser,
  updateProfile,
  fbLogin,
  logOutUser,
};

export default auth;
