import I18n from 'react-native-i18n';
import translations from './lang';

const i18n = (string, options) => I18n.t(string, options);

I18n.fallbacks = true;
I18n.defaultLocale = 'it';
I18n.locale = 'it';
I18n.translations = translations;

export default i18n;
